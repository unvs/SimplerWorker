<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
// 配置文件目录
define('CONF_PATH', __DIR__ . '/../config/');

// 绑定模块,小网站或者api使用
//define("BIND_MODULE","admin/index");// 应用只能访问admin模块下的index控制器,访问时只需要  www.tp.cc/index
//define("BIND_MODULE","admin");// 应用只能访问admin模块下的所有控制器,访问时只需要  www.tp.cc/index/index


// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
