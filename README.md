# SimplerWorker

#### 项目介绍
基于Thinkphp和workerman、gateway开发的高并发、可分布式部署的高性能及时通讯框架

#### 软件环境
PHP版本 >= 7.0   
nginx版本 >= 1.4.4   
mysql版本 >= 5.6   
centos版本建议 >= 7.0  

#### 环境搭建和安装教程
SimplerWorker部署（一）--环境搭建，地址： [https://blog.csdn.net/xkjscm/article/details/80397329]( https://blog.csdn.net/xkjscm/article/details/80397329)  
SimplerWorker部署（二）--分布式部署，地址： [https://blog.csdn.net/xkjscm/article/details/80425150]( https://blog.csdn.net/xkjscm/article/details/80425150)   
SimplerWorker部署（三）--反向代理，地址 [https://blog.csdn.net/xkjscm/article/details/80686197]( https://blog.csdn.net/xkjscm/article/details/80686197) 

    请注意： 
    1.当开发 application/worker时，启动 worker/worker_windows.bat，
        linux下时，执行 php worker/worker_server.php start -d
   
    其它文档上传到当前项目的附件里面，请到附件里面下载。


#### 说明

相对第一版本，第二版本删除了如下文件：
1. 删除了如下文件
    ./start/
    ./public/server.php
    ./application/server/
    ./vendor/jackchow/
2. RBAC 五个数据表分别在
    ./application/worker/model/Access.php 
    ./application/worker/model/Admin.php 
    ./application/worker/model/AdminRole.php 
    ./application/worker/model/Role.php 
    ./application/worker/model/RoleAccess.php
     

#### 参与贡献

1. Xiaojun.Lan Fork 本项目，初始化服务端

#### 项目计划 
1. 数据库集群
2. Redis集群
3. 选择流媒体服务器

#### 升级记录


    SimplerWorker_v1 升级到 SimplerWorker_v2：  
    升级日志：  
        1.thinkphp核心框架从5.0.19更新到5.0.20  
        2.workman核心版本从3.5.6更新到3.5.8  
        3.gateway版本从3.0.8更新到3.0.9  
        4.去除workerman/mysql，请注意，请不要在event.php中使用workerman/mysql   
                                 2018年05月28日 
[点击下载SimplerWorker_v1](https://gitee.com/SimplerWorker/SimplerWorker_v1.git)            


#### 体验交流
    QQ群：162798892    



ThinkPHP 5.0详细开发文档参考 [ThinkPHP5完全开发手册](http://www.kancloud.cn/manual/thinkphp5)

## 目录结构

~~~

SimplerWorker/
├── application                     应用目录
│   ├── admin                       后台入口（待开发）
│   │   └── controller
│   │       └── Index.php
│   ├── api                         api入口（保存一些常用函数使用方法的demo，待开发）
│   │   ├── controller
│   │   │   └── Index.php
│   │   └── model
│   │       └── User.php
│   ├── extra
│   │   └── queue.php
│   ├── index                       前台入口（保存一些常用函数使用方法的demo，待开发）
│   │   ├── controller
│   │   │   └── Index.php
│   │   └── view
│   │       ├── index
│   │       │   ├── index.html
│   │       │   └── myView.html
│   │       └── public
│   │           └── index.html
│   └── worker                      使用mvc+gatwayworker的即时通讯类入口
│       ├── controller              
│       │   ├── Events.php          业务类，这里只分配client_id，原则上不做其它业务处理
│       │   ├── Index.php           业务类，代替Events.php类
│       │   ├── Sbusinessworker.php Windows版本即时通讯的业务
│       │   ├── Server.php          Linux版本即时通讯的业务、注册、gateway
│       │   ├── Sgateway.php        Windows版本的gateway
│       │   └── Sregister.php       Windows版本的注册          
│       └── view
│           └── index
│               └── index.html      webSocket测试界面
├── build.php
├── composer.json
├── config                          配置文件 
│   ├── api                         api配置文件，只对app\api\下的文件生效
│   │   └── config.php
│   ├── config.php                  前后台、api、即时通讯配置文件
│   ├── database.php                前后台、api、即时通讯数据库配置文件
│   ├── extra                       前后台、api、即时通讯扩展配置文件
│   │   └── emails.php              
│   ├── route.php                   前后台、api、即时通讯路由配置文件
│   └── worker                      即时通讯配置文件夹
│       └── config.php              即时通讯配置文件
├── db                              数据库备份（今后将保存在docs分支中）
├── extend
├── LICENSE.txt
├── public
│   ├── api.php                     api入口文件
│   ├── favicon.ico
│   ├── index.php                   前台入口文件
│   ├── robots.txt
│   ├── router.php                  ThinkPHP的小型webserver运行文件
│   ├── worker.php                  mvc+gatewayworker即时通讯入口文件
│   └── static                      前端资源文件
├── worker                          对应 app\worker，入口文件是public\worker.php
│   ├── worker_businessworker.php   Windows版本即时通讯业务启动入口
│   ├── worker_gateway.php          Windows版本即时通讯gateway启动入口
│   ├── worker_register.php         Windows版本即时通讯注册入口
│   ├── worker_windows.bat          Windows版本服务器启动文件
│   └── worker_server.php           Linux版本即时通讯服务入口
├── README.md
├── runtime                         运行时文件，Linux下需要777权限（chmod -R 777 runtime）
├── think                           
├── thinkphp                        thinkPHP核心框架，不要改动
└── vendor                          插件目录
    ├── autoload.php                自动加载文件
    ├── composer                    composer 安装的源代码，不要改动
    ├── topthink                    插件引用，可以增加自己的，不要改动原有的
    └── workerman                   
        ├── _alidata_www_web_SimplerWorker_public_server.php.pid
        ├── gateway-worker          gateway核心源码，不要改动
        ├── gateway-worker-for-win  gateway的Windows核心源码，不要改动
        ├── workerman               workerman核心源码，不要改动
        ├── workerman-for-win       workerman的Windows核心源码，不要改动
        └── workerman.log           workerman日志

~~~

## 命名规范

`SimplerWorker`遵循PSR-2命名规范和PSR-4自动加载规范，并且注意如下规范（同Thinkphp5版本规范）：

### 目录和文件

*   目录不强制规范，驼峰和小写+下划线模式均支持；
*   类库、函数文件统一以`.php`为后缀；
*   类的文件名均以命名空间定义，并且命名空间的路径和类库文件所在路径一致；
*   类名和类文件名保持一致，统一采用驼峰法命名（首字母大写）；

### 函数和类、属性命名
*   类的命名采用驼峰法，并且首字母大写，例如 `User`、`UserType`，默认不需要添加后缀，例如`UserController`应该直接命名为`User`；
*   函数的命名使用小写字母和下划线（小写字母开头）的方式，例如 `get_client_ip`；
*   方法的命名使用驼峰法，并且首字母小写，例如 `getUserName`；
*   属性的命名使用驼峰法，并且首字母小写，例如 `tableName`、`instance`；
*   以双下划线“__”打头的函数或方法作为魔法方法，例如 `__call` 和 `__autoload`；

### 常量和配置
*   常量以大写字母和下划线命名，例如 `APP_PATH`和 `THINK_PATH`；
*   配置参数以小写字母和下划线命名，例如 `url_route_on` 和`url_convert`；

### 数据表和字段
*   数据表和字段采用小写加下划线方式命名，并注意字段名不要以下划线开头，例如 `think_user` 表和 `user_name`字段，不建议使用驼峰和中文作为数据表字段命名。


## 版权信息

SimplerWorker遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。


