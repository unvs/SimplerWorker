<?php

namespace app\index\controller;
use think\Controller;

/**
 * Created by SimplerWorker.
 * User: xiaojun.Lan
 * Date: 2018/5/4
 * Time: 18:15
 */
class Index extends Controller
{
    public function index(){

        $requestScheme = $_SERVER['REQUEST_SCHEME'];
        $domainName = $_SERVER['SERVER_NAME'];
        $url = $requestScheme."://".$domainName."/view/login.html";
        $this->redirect($url);

    }

}

