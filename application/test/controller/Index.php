<?php

namespace app\test\controller;
use think\Config;
use think\Request;
use think\Response;
use think\Controller;

/**
 * Created by SimplerWorker.
 * User: xiaojun.Lan
 * Date: 2018/5/4
 * Time: 18:15
 */
class Index extends Controller
{
    public function index(){
//        var_dump(Config::get('connector'));
        var_dump(Config::get('database'));
    }

    /**
     * 参数传递,原始路由为:
     *  www.tp5.cc/index/index/info/id/5.html  (.html可省略)
     *  或者www.tp5.cc/index.php/index/info/id/5.html (.html可省略)
     *  ,要把路由改为: www.tp5.cc/info/5.html,需要在配置中配置:
     *          'url_route_on'           => true,// 是否开启路由
     *          'url_route_must'         => true, // 是否强制使用路由
     *          且新建/config/route.php配置文件,配置路由 'info/:id' => 'index/index/info',
     * @param $id string 需要传入的值
     * @return mixed 输出的值
     */
    public function info($id){
        echo url('index/index/info',['id'=>10]).'</br>';// 打印 /info/10.html
        echo url('info',['id'=>10]).'</br>'; // 打印 /index/index/info/id/10.html
        return "{$id}";
    }

    /**
     * 获取request对象方法1
     */
    public function request1(){
        $request = request();
        dump($request);
    }
    /**
     * 获取request对象方法2
     */
    public function request2(){
        $request = Request::instance();
        dump($request);
    }

    /**
     * 获取request对象方法3
     * @param Request $request
     */
    public function request3(Request $request){
        dump($request);
    }

    public function requestTest(Request $request){
        dump($request->domain());
        dump($request->pathinfo());
        dump($request->path());
        //请求类型
        dump($request->method());
        dump($request->isGet());
        dump($request->isPost());
        dump($request->isAjax());
        //请求参数
        dump($request->get());
        dump($request->post());
        dump($request->param());// 获取 post \ get 的所有值
        dump(input('post.id'));// 获取post传递过来的id
        dump(input('get.id'));// 获取get传递过来的id
        dump(input('post.id',100)); // 通过input获取post传过来的id,如果id不存在,默认100
        dump(input('post.id',100,'intval'));// 将input的post获取的id转为整形
        dump($request->get('id',99,'intval'));// 通过request的post方法获取id,并强转为整形
        //coolie和session
        session('name','');
        dump($request->session());
        cookie('email','18888@qq.com');
        dump($request->cookie());

        //获取值
        dump($request->param('type'));
        dump($request->cookie('email'));

        //获取当前模块\控制器\操作
        dump($request->module());
        dump($request->controller());
        dump($request->action());

        dump($request->url());
        dump($request->baseUrl());

    }

    /**
     * 通过ajax请求来获取值
     * @return array
     */
    public function ajaxRequest(Response $response){
        $res = [
            'code'=>200,
            'result'=>[
                'list'=>[1,2,3,4,5,6]
            ]
        ];
        Config::set( 'default_return_type', 'xml');
       return $res;
    }

    public function myView(){
        # 默认模板地址
        # app/index/view/index/index.html
//       return view('myView');
//        return view('public/index');
        # 如果以 ./ 开头 , 那么找到入口文件统计开始的模板文件
//        return view('./index.html');
        # 带参数
        /*return view('index',
            ['email'=>'222222@qq.com','name'=>'xiaojun.lan'],
            ['STATIC'=>'当前是static的替换内容']);*/
        #
        return $this->fetch('index');

    }

}

