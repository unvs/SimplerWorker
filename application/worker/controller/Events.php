<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/5/17
 * Time: 9:29
 */

namespace app\worker\controller;
use think\Controller;
/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);

use \GatewayWorker\Lib\Gateway;

class Events  extends Controller{


    /**
     * 当有客户端连接时，将client_id返回，让mvc框架判断当前uid并执行绑定
     * @param $client_id string 用户id
     */
    public static function onConnect($client_id)
    {

        Gateway::sendToClient($client_id, json_encode(array(
            'type' => 'init',
            'client_id' => $client_id
        )));

    }

    /**
     * 进程启动后，这里不再支持数据库操作，业务请移至thinkPHP框架内操作
     */
    public static function onWorkerStart($worker){
    }

    /**
     * onMessage建议不做任何业务逻辑,业务逻辑由thinkPHP来做
     * @param $client_id
     * @param $message
     * @return bool|void
     */
    public static function onMessage($client_id, $message){
    }

}
