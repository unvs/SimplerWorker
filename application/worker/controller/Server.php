<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/5/15
 * Time: 17:10
 */

namespace app\worker\controller;

use Workerman\Worker;
use GatewayWorker\Gateway;
use GatewayWorker\Register;
use GatewayWorker\BusinessWorker;

class Server
{

    /**
     * 构造函数
     * @access public
     */
    public function __construct(){

        $info = include_once dirname(dirname(dirname(dirname(__FILE__))))."/config/config.php";

        $register = $info["is_register"];// 是否将当前服务器配置为 register 服务器
        $registerIpPort = $info["register_ip_port"];// gatewayworker的webSocket注册地址

        $businessWorker = $info["business_worker"];// 是否将当前服务器配置为 business_worker 服务器
        $businessWorkerCount = $info["business_worker_count"];//  当前business_worker服务器开启的进程数
        $eventHandler = $info["event_handler"];
        $WorkerBusinessName = $info['Worker_business_name'];

        $gateway = $info["gateway"] = true;// 是否将当前服务器配置为 gateway 服务器
        $gatewayLanIp = $info["gateway_lan_ip"];// 当前 gateway 服务器ip地址（内网ip）
        $gatewayWebSocket = $info["gateway_web_socket"];// 服务器webSocket地址和端口，提供客户端连接，一般是当前gateway服务器外网ip和端口
        $gatewayCount = $info["gateway_count"]; // 当前gateway服务器的进程数
        $gatewayStartPort = $info["gateway_start_port"];/*当前gateway服务器的内部通讯起始端口,如果gateway_count为4，
                                                        则一般会使用2300 2301 2302 2303 4个端口作为内部通讯端口*/
        $gatewayName = $info['gateway_name'];// gatewayName

        if($register){
            new Register('text://'.$registerIpPort);
        }

        if($businessWorker){
            $worker = new BusinessWorker();
            $worker->name = $WorkerBusinessName;
            $worker->count = $businessWorkerCount;
            $worker->registerAddress = $registerIpPort;
            $worker->eventHandler = $eventHandler;
        }

        if($gateway){
            $gateway = new Gateway("websocket://".$gatewayWebSocket);
            $gateway->name = $gatewayName;
            $gateway->count = $gatewayCount;
            $gateway->lanIp = $gatewayLanIp;
            $gateway->startPort = $gatewayStartPort;
            $gateway->pingInterval = 10;
            $gateway->pingData = '{"type":"ping"}';
            $gateway->registerAddress = $registerIpPort;
        }

        //运行所有Worker;
        Worker::runAll();

    }

}
