<?php
/**
 * Created by yd
 * User: xiaojun.lan
 * Date: 2018/5/31
 * Time: 19:21
 */

namespace app\api\model;


use think\Model;

class RoleAccess extends Model
{


    /*
     *
        CREATE TABLE `sw_role_access` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色id',
            `access_id` varchar(500) NOT NULL DEFAULT '0' COMMENT '权限id列表',
            `create_time` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
            `update_time` int(11) unsigned DEFAULT NULL COMMENT '最后一次更改时间',
            PRIMARY KEY (`id`),
            KEY `idx_role_id` (`role_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限表'
     *
     */

    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';


    /**
     * 将要插入库的权限id列表编码
     * @param $val
     * @return string
     */
    public function setAccessIdAttr($val){
        return trim($val,',');
    }

    /**
     * 将查询的权限id列表进行解码
     * @param $val
     * @return mixed
     */
    public function getAccessIdAttr($val){
        return trim($val,',');
    }

    /**
     * 将查询到的创建时间戳转换为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getCreateTimeAttr($val){
        return empty($val)?'':date('Y-m-d H:i:s', $val);
    }

    /**
     * 将查询到的更新时间转换为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getUpdateTimeAttr($val){
        return empty($val)?'':date('Y-m-d H:i:s', $val);
    }

}

