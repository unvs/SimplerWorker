<?php

namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;


/**
 * Created by xiaojun.lan
 * User: xiaojun.lan
 * Date: 2018/5/28
 * Time: 14:53
 */
class Admin extends Model
{

    #   命名 sw_admin 对应 User.php
    #   假设: sw_user_info 则对应 UserInfo.php文件

    /*
       CREATE TABLE `sw_admin` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
            `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
            `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
            `phone` varchar(30) DEFAULT NULL COMMENT '电话（座机）',
            `mobile` varchar(30) DEFAULT NULL COMMENT '手机号',
            `sex` tinyint(1) NOT NULL DEFAULT '2' COMMENT '性别:0,女；1,男；2,未知',
            `is_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是超级管理员 1:是; 0:不是',
            `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1:有效;0:无效',
            `comment` varchar(50) DEFAULT NULL COMMENT '行备注',
            `create_time` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
            `update_time` int(11) unsigned DEFAULT NULL COMMENT '更新时间',
            `delete_time` int(11) unsigned DEFAULT NULL COMMENT '删除时间',
            PRIMARY KEY (`id`),
            KEY `idx_email` (`email`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表（id为1的管理员不可删除）';

    */

    use SoftDelete;
    protected $autoWriteTimestamp = true;
    // 表中需要存在 create_time int类型的字段，
    //如果要更改名，可以设置为 protected $createTime = 'create_at'；
    //如果默认不要插入，设置为  protected $createTime = false;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time'; // 同上
    protected $deleteTime = 'delete_time'; // 同上


    /**
     * 获取管理员状态
     * @param $val
     * @return string
     */
    public function getStatusAttr($val)
    {
        switch ($val) {
            case '1':
                return '有效';
                break;
            case '0':
                return '无效';
                break;
            default:
                return $val;
                break;
        }
    }

    /**
     * 设置管理员状态
     * @param $val
     * @return string
     */
    public function setStatusAttr($val)
    {
        switch ($val) {
            case '有效':
                return '1';
                break;
            case '无效':
                return '0';
                break;
            default:
                return (int)$val === 0 ? 0 : 1; // 默认有效
                break;
        }
    }

    /**
     * 模型中的 getSexAttr 方法名是固定的,将会判断数据库中sex字段的值,然后返回相应的替换值
     * @param $val
     * @return string
     */
    public function getSexAttr($val)
    {
        switch ($val) {
            case '0':
                return '男';
                break;
            case '1':
                return '女';
                break;
            case '2':
                return '未知';
                break;
            default:
                return $val;
                break;
        }
    }

    /**
     * 自动替换性别为相应数字
     * @param $val
     * @return int
     */
    public function setSexAttr($val)
    {
        switch ($val) {
            case '男':
                return 0;
                break;
            case '女':
                return 1;
                break;
            case '未知':
                return 2;
                break;
            default:
                return in_array((int)$val, [0, 1, 2]) ? (int)$val : 2; // 强转为整形1,2,3，避免插入数据异常
                break;
        }
    }

    /**
     * 设置是否是管理员
     * @param $val
     * @return int
     */
    public function setIsAdminAttr($val)
    {
        switch ($val) {
            case '是':
                return 1;
                break;
            case '不是':
                return 0;
                break;
            default:
                return (int)$val === 1 ? 1 : 0;
                break;
        }
    }

    /**
     * 获取是否为管理员,返回1或0，便于应用判断，不转化为是、不是
     * @param $val
     * @return string
     */
    public function getIsAdminAttr($val)
    {
        switch ($val) {
            default:
                return $val;
                break;
        }
    }

    /**
     * 自动将用户传递进来的密码进行MD5加密，然后返回保存到数据库
     * @param $val
     * @return string
     */
    public function setPassWordAttr($val)
    {
        return md5($val);
    }

    /**
     * 将查询到的更新时间戳转换为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getUpdateTimeAttr($val)
    {
        return empty($val) ? '' : date('Y-m-d H:i:s', $val);
    }

    /**
     * 将查询到的创建时间戳转换为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getCreateTimeAttr($val)
    {
        return empty($val) ? '' : date('Y-m-d H:i:s', $val);
    }

    /**
     * 将查询到的软删除事件戳转为日期时间格式
     * @param $val
     * @return false|string
     */
    public function getDeleteTimeAttr($val)
    {
        return empty($val) ? '' : date('Y-m-d H:i:s', $val);
    }

}

